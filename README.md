# Vision eines idealen Open Science Projektes

Hier nur ein paar Ideen

* Offenheit
* Nachnutzbarkeit
* Nachvollziehbarkeit
* Partizipationsmöglichkeit
* Qualitätssicherung
* Maschinenlesbarkeit
* Semantische Vernetzung
* ...

Dieses Projekt basiert auf der [OER Kursvorlage]().

* [Dokument als Ebook](https://axel-klinger.gitlab.io/vision-eines-idealen-open-science-projekts/course.epub)
* [Dokument als PDF](https://axel-klinger.gitlab.io/vision-eines-idealen-open-science-projekts/course.pdf)
* [Dokument als HTML](https://axel-klinger.gitlab.io/vision-eines-idealen-open-science-projekts/index.html)
* [Präsentation als revealjs]()

Präsentation lokal starten mit [reveal-md](https://github.com/webpro/reveal-md)

```
npm install -g reveal-md
```
```
reveal-md slides/idea.md
```
