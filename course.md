# Das ideale Projekt im Sinne von Open Science

## … auch für Publikationen …

* ideale Darstellung, Nachvollziehbarkeit und Partizipationsmöglichkeit für Interessierte
* transparent und nachnutzbar - offen lizenziert

Eigentlich hat die ganze Idee damit begonnen, eine ideale Form für freie Bildungsmaterialien zu finden. Später kamen dann auch wissenschaftliche Publikationen dazu. In den letzten Jahren kamen mit der Einbeziehung von Forschungsdaten und Software weitere Komponenten für Publikationen dazu, die eine Erweiterung des Rahmens auf Projekte rechtfertigen. Neben den Artefakten der Publikation werden im folgenden auch ergänzende Konzepte berücksichtigt, wie z.B. die Versionierung, die Partizipation auf verschiedenen Ebenen, die Dokumentation des Fortschritts und der Überlegungen zu den einzelnen Abschnitten, die transparente Planung der Ausarbeitung mit Priorisierung, eine kontinuierliche Qualitätskontrolle, semantische und technische Annotationen u.v.a.m., weshalb sich der Begriff des Projekts besser eignet als die reine Publikation.

## Umfang

* Dokumente, Daten, Software, Personen, Events, Planung, Aufgaben, Dokumentation, … so weit es geht mit PIDs (mandatory!?!)

Wie bereits oben genannt und nicht zuletzt in den Diskussionen um Forschungsdatenmanagement thematisiert, besteht eine vollständige Publikation nach heutigen Vorstellungen nicht nur aus dem reinen Textdokument in Form eines PDFs, wie es bisher üblich war, sondern umfasst allein bezüglicher Artefakte auch Daten, Software und ggf. noch weitere Elemente. Neben den Artefakten spielen auch die Metadaten eine wesentliche Rolle für die Auffindbarkeit in einer stetig wachsenden Menge von Publikationen in allen Bereichen der Wissenschaft.

## Format

* optimale Darstellung, interaktive Elemente und Bedienung auf allen Geräten - vom Smartphone über Tablet bis PC

PDFs sind eine Publikationsform des letzten Jahrhunderts! Smartphones und Tablets, die heutzutage zu den gängigen Lesegeräten zählen wurden erst Anfang des 21. Jahrhunderts erfunden und sind bei den heutigen Generationen die bevorzugten Interaktionsgeräte.

Publikationen in Form von PDFs sind in der Regel auf DIN A4 ausgerichtet und nicht für 6, 7, 8 oder 10" Geräte geeignet. Wer schon einmal versucht hat, ein PDF auf einem Smartphone zu lesen, weiß was ich meine.

Neuere offene Publikationsformen, wie z.B. eBooks bieten zahlreiche Vorteile:

* bei der Erstellung muss ich nicht auf Formatvorlagen achten und kann mich auf die Inhalte konzentrieren, da ich nur reine Textdokumente in Markdown, oder bei mehr Features in AsciiDoc oder LaTeX verfasse  
* die Leser können selbst wählen zwischen Schriftgröße, Schriftart, Hintergrund etc. und haben damit den für sie bestmöglichen Lesekomfort
* reine Textdokumente ohne Formatierung sind einfacher maschinenlesbar, wodurch sich auch einfacher Qualitätskontrollen umsetzen lassen (mehr dazu später ...)

## Metadaten

* enthalten, geprüft und konform zu schema.org/CreativeWork

* PIDs sollten eine wesentliche Rolle in jeder Publikation spielen
  - Unterstützung bei der Erstellung

Beispiel: wenn die Metadaten bereits im Dokument in sauberer Form enthalten sind bringt das zahlreiche Vorteile, wie z.B.

* bei der Veröffentlichung in einem Repository reicht allein die URL/der DOI des Dokuments aus, um alle Metadaten automatisch zu füllen, sofern das Repository diese aus dem Header interpretieren kann (Beispiel twillo und "Gitlab für Texte")
* in der Google-Suche werden diese Publikationen besonders hoch geranked (Beispiel "Gitlab für Texte")
* die Metadaten liegen in der Hand des Erstellers
* -> per QA und Formular (mit dahinter liegenden Terminologien) lassen sich die Metadaten allein aus einem Template benutzerfreundlich und standardisiert erstellen!

-> eine QA via CI im Template könnte auf einfache Weise sicher stellen, dass eine Publikation nur dann anerkannt wird, wenn z.B. alle Zitationen per DOI erfolgen, alle Autoren mit (gültigen!) ORCIDs angegeben sind, bestimmte Richtlinien erfüllt sind und sämtliche minimal aber geforderte Metadaten nach schema.og/CreativeWork vollständig gesetzt sind

**Unterstützung**

* wenn ich die ORCID eines Autors eingebe wird automatisch der Name (und was vielleiht noch?) eingefügt
  - Autoren sind die Contributoren des Dokuments -> wenn diese im GitLab Projekt eingetragen sind, können sie dann automatisch eingfügt werden? Kann ein GitLabb-User seine ORCID hinterlegen?
* wenn ich ein Fachgebiet angebe wird automatisch der passende Thesaurus(?) für die Vergabe von standardisierten Schlagworten geladen, die beim Tippen vervollständigt und mit Enter übernommen werden können
  - oder lassen sich diese schon aus dem Dokument extrahieren bzw. die Extraktion unterstützen?
* der Titel wird aus dem Titel des Projekts übernommen
* das Veröffentlichungsdatum wird aus dem Release/Tag übernommen
* die Institution wird pro Autor (wenn vorhanden aus dem ORCI gezogen)
* wie lassen sich die Namen/IDs von Veranstaltungen integrieren? über Tags?

## Qualitätskontrolle

Am Beispiel einer statischen Code Analyse in der Software Entwicklung:

**Vergleich einiger Regeln:**

Einfache Struktur bzw. Markdown Parser

* zu lange Methoden - zu lange (und damit potentiell unverständliche) Sätze
* leere Methoden - Überschriften ohne Absätze bzw. Abschnitte ohne Inhalte

Auf Basis eines Abstract Syntax Tree für die deutsche (und weitere) Sprache

* überlagerte Variablen - Mehrdeutigkeiten
* ... Zusammenhang des Textes durch Analyse des Kontextgraphen ...
* Regeln für gute Texte, wie z.B. https://www.google.com/search?q=Regeln+für+gute+texte
  - Subjekt und Verb nach vorne!
  - Nominalkonstruktionen vermeiden!
  - Passivsätze vermeiden. ...
  - Füllwörter vermeiden!
  - Schachtelsätze vermeiden.

Durch Validierung gegen Grundwortschatz und Fachwortschatz
* undefinierte Variablen - Begriffe, die weder im Grundwortschatz, noch im Fachvokabular definiert sind und nicht in der Form auftauchen "ein X ist ein ..."


**Unterstützung**

* Auswertungen mit Empfehlungen für Verbesserungen in Sinne einer statischen Codeanalyse wären ein erster, einfacher Schritt und können modular und einfach weiterentwickelt und erprobt werden
* langfristig ist dabei anzustreben, dass Autoren im Editor in Echtzeit Feedback bekommen, was sie verbessern können - je früher man einen Fehler entdeckt, desto günstiger ist er zu beheben! d.h. z.B. wenn ich einen zu langen Satz formuliere sollte ich idealerweise gleich im Editor sehen, dass er suboptimal ist -> über SonarLint sollte das bereits mit Verbindung zum SonarQube Server in VS Code möglich sein!

## Partizipation

* über Feedback Formular, Aufgaben, Pull Requests, …

Bisher war es so, dass man eine Publikation in einem Repository eingereicht hat, und anschließend irgendwo über die guten und schlechten Stellen darin berichtet wurde. Es fehlt damit jeglicher Rückkanal für konstruktives Feedback. In der Open Source Softwareentwicklung werden Anmerkungen zu einem Produkt (vergleichbar mit einer Publikation) in Form von Issues angelegt, die dann abgearbeitet und abgehakt werden können. In gleicher Weise könnten auch wissenschaftliche Arbeiten offen und nachhaltig verbessert werden.

## Semantische Anreicherung

Aus meiner Sicht noch offen ist die Frage, was an welchen Stellen ergänzt werden sollte, damit semantisch relevante Stellen maschinenlesbar sind. Wenn dies definiert ist, können wie bei der QA Vorschläge generiert werden.

## Versionierung

Die Versionierung, wie sie schon in der Software Entwicklung bekannt ist, könnte bei Publikationen oder wissenschaftlichen Projekten so aussehen, dass

* ein Artikel, der immer wieder neuen Erkenntnissen oder Arbeitsschritten angepasst ist, im gleichen Dokument diese als Versionen oder Tags kenntlich macht, z.B. durch Taggen mit den IDs von Konferenzen
* Branches/Verzweigungen oder Alternativen/Varianten bieten die Möglichkeit verschiedene Ansätze zu erproben, z.B. für parallel gestellte Anträge mit unterschiedlicher Ausrichtung
* Tags/Releases könnten z.B. im Kontext von Bildungsmaterialien mit den Semestern gekennzeichnet sein, in denen sie zur Anwendung kamen und erweitert wurden

## Features

* QA mit Verbesserungsvorschlägen (s. Sonar in Open Source)
* Semantische Anreicherung (s. ORKG) -> Mehrwert für ORKG!
* Unterstützung bei Zitationen (s. ORKG)
* attraktive interaktive Ausgabeformate wie eBook (s. Gitlab für Texte oder Studienarbeit Fassade)
* bessere Auffindbarkeit durch inkludierte Metadaten (s. „Gitlab für Texte“ oder „Studienarbeit Fassade“) -> goolge mal ;-)
* einfache Veröffentlichung auf diversen Plattformen durch inkludierte und validierte Metadaten (s. Gitlab für Texte in twillo) - auch in DSpace, CKAN denkbar, gerade wenn der Standard schema.org/CreativeWork ist - auch als Pull Request denkbar
* Versionierung
* Transparenz über die Beteiligung der Mitwirkenden
* Archivierbarkeit über Clone & Sync
* einfaches Rechtemanagement
* in Standard Open Source Software zur digitalen Souveränität

## Fazit?

Die meisten der hier vorgestellten Features sind bereits heute mit einer vorhanden offenen Lösung auf Basis langjährig erprobter Werkzeuge in der Open Source Software Entwicklung verfügbar. Größere Herausforderungen, für die sich zur Beisteuerung zu der vorhandenen Umgebung ggf. auch internationale Anträge lohnen würden, sind u.a. bzw. vor allem:

* ein einfache Editor für synchrones Schreiben in der Form von HackMD/CodiMD/HedgeDoc
* ein um Regeln erweiterbarer QA Server für Texte in der Form von SonarQube/Cloud

Zukunftsfähige Texte lassen sich bereits heute mit gängigen Mitteln erstellen, ohne dass wir auf eine neue Erfindung warten müssten oder abhängig wären von der Entwicklung einer nationalen, teuer finanzierten Speziallösung, die dann (ohnehin) nicht genutzt wird, weil sie a) global gesehen eine Insel ist b) nicht weit genug gedacht wird c) nicht international ist d) nicht offen genug nachnutzbar ist e) nicht skaliert f) sie keiner weltweit offen hostet g) ...

Wir müsse für richtig gutes Open Science nicht das Rad neu erfinden und eine eigene Software entwickeln. Mit Templates in den Werkzeugen für Open Source Software Entwicklung läßt sich die Anforderung für Publikationen in Form von Artikeln, Bildungsmaterialien, Präsentationen, Konferenzbeiträgen, Abschluss- oder Projektarbeiten bereits leichtgewichtig adhoc umsetzen.
