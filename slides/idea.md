---
theme : solarized
#title: Vision für Open Science Projekte/Publikationen
#author: Axel Klinger
#date: 27. November 2021
revealOptions:
  transition: slide
  slideNumber: false
  history: true
---

## Open Science Project Template

**Axel Klinger**

*20. Dezember 2021*

---

# Inhalt

- Probleme
- Ziele
- Ansatz
- Offene Punkte
- Arbeitspakete

---

# Probleme

----

## Forschung ist intransparent

In vielen Fällen wird ein Forschungsvorhaben durchgeführt und am Ende ein Bericht geschrieben, aber die einzelnen Schritte bzw. Eingangsdaten und Ergebnisse sind nicht nachvollziehbar.

----

## Publikationen sind statisch und Papier orientiert

* PDFs (20. Jahrhundert) vs. Smartphones/Tablets (21. Jahrhundert)
* fehlende Versionierung

----

## Auffindbarkeit

... ist abhängig von Plattformen und Qualität der Metadaten

----

## fehlende Partizipationsmöglichkeiten

* ...

----

## fehlende Maschinenlesbarkeit

* vorhandene Texte sind nicht semantisch ausgezeichnet
* Texte sind zu konfus für automatische Erschließung z.B. im ORKG

---

# Ziele

----

## Attraktiviere Formate

* eBooks bieten den Lesenden die Möglichkeit, Schriftart und Schriftgröße selbst zu bestimmen und funktionieren auf Smartphones wie Tablets oder Desktops in optimaler Form

----

## Bessere Auffindbarkeit

* inkludierte Metadaten bieten hohes Ranking bei Google
* erleichtern die Einreichung in Publikationsplattformen
* sorgen bei impliziter Kontrolle für bessere Qualität

----

## Höhere Qualität

- Unterstützung bei der Erzeugung
- Annahme durch standardisierte QA-Berichte

----

## Maschinenlesbarkeit

* Reintext
* semantische Anreicherung

----

## Nachnutzbarkeit

- per Clone
- Zitation über PIDs

----

## Transparenz

- Beteiligte
- Änderungen und Versionen

----

## Partizipation

- Anmerkungen (per Formular)
- Diskussionen (in Issues)
- Verbesserungsvorschläge (Pull Requests)
- Beteiligung (per Berechtigung)

---

# Ansatz

----

## Wahl der Mittel

- hier: GitLab Projekte + Templates mit CI

---

# Fragen?

----

## Kontakt

* axel.klinger@tib.eu
